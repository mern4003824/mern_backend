## Dependencias instaladas y su propósito

1. `dotenv`: dependencia que permite utilizar los .env
2. `express`: Framework para crear un backend en node.js
3. `npm` : NPM es el administrador de paquetes predeterminado para Node.js, para instalar y mantener las dependencias del proyecto

## DevDependencies instaladas y su propósito

1. `@types/express`: Proporciona los tipos TypeScript para Express, lo que ayuda con la validación del código y la autocompletación en el entorno de desarrollo.
2. `@types/jest`: Proporciona los tipos TypeScript para Jest, lo que ayuda con la validación del código y la autocompletación en el entorno de desarrollo.
3. `@types/node`: Proporciona los tipos TypeScript para Node.js, lo que ayuda con la validación del código y la autocompletación en el entorno de desarrollo.
4. `@typescript-eslint/eslint-plugin`: Un complemento de ESLint que permite el uso de reglas específicas de TypeScript.
5. `concurrently` : Permite ejecutar comandos de forma consecutiva, de esta forma crear comandos "scripts" más robustos.
6. `eslint` : Una herramienta de linting para JavaScript y TypeScript.
7. `eslint-config-standard-with-typescript` : Un conjunto de reglas ESLint para proyectos que utilizan TypeScript y siguen el estilo de código estándar.
8. `eslint-plugin-import` : Un complemento de ESLint que proporciona reglas para la importación y exportación de módulos.
9. `eslint-plugin-n` : Un complemento de ESLint para evitar nombres de variables confusos.
10. `eslint-plugin-promise` : Un complemento de ESLint para verificar el uso correcto de las promesas.
11. `jest` : Un marco de pruebas para JavaScript y TypeScript.
12. `nodemon` : herramienta para correr el código node.js y detectar cambios para recargar el código en vivo (util para debug).
13. `serve` : Un servidor web estático para servir archivos estáticos o informes generados.
14. `supertest` : Una biblioteca de aserciones HTTP para pruebas de API.
15. `ts-jest` : Un complemento de Jest para admitir pruebas de TypeScript.
16. `ts-node` : Ejecuta archivos TypeScript directamente en Node.js sin necesidad de compilación previa.
17. `typescript` : Un superset de JavaScript que agrega tipos estáticos opcionales.
18. `webpack` : Un empaquetador de módulos estáticos para JavaScript.
19. `webpack-cli` : Interfaz de línea de comandos para Webpack.
20. `webpack-node-externals` : Excluye los módulos de Node.js del paquete final de Webpack.
21. `webpack-shell-plugin` : Un complemento para Webpack que ejecuta comandos de shell antes o después de la compilación.

## Variables .env

1. `PORT`: con este variable de environment, listamos el puerto a escuchar las rutas definidas en la aplicación.

## Scripts de NPM y su funcionalidad

1. `build`: Ejecuta el comando `npx tsc`, que compila el código TypeScript en JavaScript en la carpeta `dist/` (según configuración en tsconfig.json).
2. `start`: Inicia la aplicación Node.js ejecutando el archivo `dist/index.js` (el cual fue compilado desde index.ts) con el comando `node`.
3. `fund`: Ejecuta el comando `npm fund`, que muestra información sobre cómo apoyar financieramente a los paquetes utilizados en tu proyecto.
4. `dev`: Utiliza el paquete `concurrently` para ejecutar dos comandos simultáneamente. `npx tsc --watch` observa los cambios en los archivos TypeScript y los compila automáticamente, mientras que `nodemon -q dist/index.js` reinicia el servidor de forma automática cuando se detectan cambios en los archivos de la carpeta `dist/`.
5. `test`: Ejecuta las pruebas utilizando el marco de pruebas Jest.
6. `serve:coverage`: Ejecuta las pruebas con Jest y luego abre un servidor web local utilizando el paquete `serve` para mostrar el informe de cobertura de pruebas ubicado en la carpeta `coverage/lcov-report`.