import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

//variables de seguridad
import cors from 'cors';
import helmet from 'helmet';

// TODO: HTTPS
import router from '../routes';


//TODO: Moongoose Connection

dotenv.config();    
const server: Express = express();

server.use('/api', router);

// Define server to use "/api" and use rootRouter
server.use(helmet());
server.use(cors());
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

//http localhost:8000/ --> http://localhost:8000/api
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
});

export default server;

