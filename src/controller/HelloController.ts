import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { LogSuccess } from "../utils/logger";


/**
 * 
 */
export class HelloController implements IHelloController {
    /**
     * 
     * @param name 
     * @returns BasicResponse
     */
    public async getMessage(name?: string): Promise<BasicResponse> {
        //throw new Error("Method not implemented.");
        LogSuccess('[/api/hello] Get Request');

        return {
            message: `Hello ${name || 'World'}`
        };
    }


}