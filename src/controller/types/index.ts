/**
 * Basic JSON Response Object for controllers
 */
export type BasicResponse = {
    message: string;
}

/**
 * Basic Json Error Response Object for controllers
 */
export type ErrorResponse = {
    error: string,
    message: string;
}