import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";
import { BasicResponse } from "../controller/types";

// Router from Express

let helloRouter = express.Router();


// Get -> http://localhost:8000/api/hello?name=Oscar/
// como fuimos redirigidos, el /, pasaría ser la redirección + la nueva ruta
helloRouter.route('/')
    // Get:
    .get(async (req: Request, res: Response) => {
        // obtain a Query param
        let name: any = req?.query?.name;
        LogInfo(`Query Param: ${name}`);
        // Controller Instance
        const controller: HelloController = new HelloController();
        //cuando se hace la llamada al controller se coloca el await
        const response: BasicResponse = await controller.getMessage(name);
        // Send response to client
        return res.send(response);
    })


//export hello router
export default helloRouter;

