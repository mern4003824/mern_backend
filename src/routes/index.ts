/**
 * Root Router
 * Redirections to Routers
 */

import express, { Express, Request, Response } from "express";
import helloRouter from "./HelloRouter";

// server instance
let server: Express = express();

// Router Instance
let rootRouter = express.Router();


/**
 * Routes for http://localhost:8000/api
 */

rootRouter.get('/', (req: Request, res: Response) => {
    res.send("hello world");
})

//redirections to routers


server.use("/", rootRouter); // http://localhost:8000/api
server.use('/hello', helloRouter); // http://localhost:8000/api/hello --> HelloRouter (pasaría ser el /)


export default server;