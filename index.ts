import dotenv from 'dotenv'
import server from './src/server'
import { LogSuccess, LogError } from './src/utils/logger'

dotenv.config();
const port = process.env.PORT || 8000;


// * Execute server
server.listen(port, () => {
  LogSuccess(`Server is listening on ${port}/api`);
});

// * Control Server Error 

server.on('error', (error) => {
  LogError(`[SERVER ERROR] ${error}`);
});